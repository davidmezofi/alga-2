\chapter{Visszalépéses keresés, korlátozás-szétválasztás elve}
%------------------------------------------------------------------------------
Legyen $L$ egy véges halmaz és $z\colon L\rightarrow\mathbb{R}$ egy függvény, a
\emph{célfüggvény}. Mennyi $\min\left\{z{\left(x\right)}\colon x\in L\right\}$,
és hol veszi fel $z$ ezt a szélsőértéket? Mivel $L$ véges, ezért a fenti
feladatnak biztosan van megoldása. A legegyszerűbb megoldás: végignézzük $L$
minden elemét, és kiválasztunk egy olyat, melyre $z{\left(x\right)}$ minimális.
Ezt nevezik leszámlálási eljárásnak. Csakhogy $L$ elemszáma nagyon nagy lehet!
%------------------------------------------------------------------------------
\minisec{Korlátozás és szétválasztás módszere (Branch and Bound, B\&B)}
A leszámlálás egy hatékonyabb változata, általában NP-nehéz feladatoknál
szokták használni. Legfontosabb elemei:
\begin{enumerate}[nosep]
  \item szétválasztó függvény;
  \item korlátozó függvény;
  \item levélválasztási stratégia.
\end{enumerate}
\begin{mydefin}[szétválasztó függvény]
  Az $L$ halmaz tetszőleges $\left|L'\right|>1$ részhalmazához hozzárendeli
  $L'$ egy valódi osztályozását. A szétválasztó függvényt $\phi$-vel fogjuk
  jelölni és egy fával ábrázolható.
\end{mydefin}

Sok esetben olyan a feladat, hogy $L$ vektorok halmaza, tehát minden
$\mathbf{x}\in L$ esetén
$\mathbf{x}=\left(x_{1}, x_{2}, \ldots, x_{n}\right)$. Ha
$x_{i}\in\left\{a_{i_{1}}, a_{i_{2}}, \ldots, a_{i_{k}}\right\}$, akkor
természetesen adódik az $i$-ik szinten az $x_{i}$ értéke szerinti
szétválasztás.

Van azonban egy olyan probléma, hogy sok esetben nehéz megfelelő szétválasztó
függvényt definiálni. Ezért általában egy $\Omega\supseteq L$ halmazból
szoktunk kiindulni, és arra definiáljuk a szétválasztó függvényt.
\begin{mydefin}[korlátozó függvény]
  Egy olyan $g$ függvény, amely $L$ tetszőleges $L'\neq\emptyset$
  részhalmazához hozzárendeli a $\left\{z{\left(x'\right)}\colon x'\in
  L'\right\}$ függvényértékek egy alsó korlátját, továbbá ha
  $L'=\left\{x'\right\}$, akkor $g{\left(L'\right)}=z{\left(x'\right)}$.
\end{mydefin}
%------------------------------------------------------------------------------
\minisec{A B\&B eljárás}
Az eljárás során $z^*$-gal fogjuk jelölni az adott pillanatig megtalált legjobb
optimum értéket, és $x^{*}$-gal az ehhez tartozó lehetséges megoldást, azaz
amire $z{\left(x^{*}\right)}=z^{*}$.
\begin{enumerate}
  \item Heurisztikus módszerrel határozzunk meg egy minél jobb meg oldást,
    legyen ez $x^{*}$, és $z^{*}=z{\left(x^{*}\right)}$.
  \item Határozzuk meg $g{\left(\Omega\right)}$-t. Ennek során minden
    esetlegesen előálló $x'$ lehetséges megoldásra, ha
    $z{\left(x'\right)}<z^{*}$, akkor legyen $x^{*}=x'$ és
    $z^{*}=z{\left(x'\right)}$. Legyen $F_{0}=\left\{\Omega\right\}$, ha
    $g{\left(\Omega\right)}<z^{*}$, és $F_{0}=\emptyset$ különben. Legyen
    $r=0$.
  \item Ha $F_{r}=\emptyset$, akkor $x^{*}$ optmális megoldás, $z^{*}$ az
    optimum értéke és vége az eljárásnak. Egyébként folytassuk.
  \item Valamilyen \emph{levélválasztási stratégia} szerint válasszunk ki egy
    $\Omega'\in F_{r}$ elemet. Legyen
    $\phi{\left(\Omega'\right)}=\left\{\Omega_{1}', \Omega_{2}', \ldots,
    \Omega_{k}'\right\}$. Határozzuk meg rendre a $g{\left(\Omega_{1}'\right)},
    g{\left(\Omega_{2}'\right)}, \ldots, g{\left(\Omega_{k}'\right)}$
    korlátokat. A korlátok meghatározása során minden esetlegesen előálló $x'$
    lehetséges megoldásra, ha $z{\left(x'\right)}<z^{*}$, akkor legyen
    $x^{*}=x'$ és $z^{*}=z{\left(x'\right)}$.

    Legyen
    \begin{equation}
      F_{r+1} = \left\{\Omega_{i} \colon \Omega_{i} \in \left(F_{r} \setminus
      \left\{ \Omega' \right\} \right) \cup \phi{\left(\Omega'\right)} \text{
      és } g{\left(\Omega_{i}\right)}<z^{*}\right\}.
    \end{equation}
    Növeljük $r$ értékét 1-gyel, és térjünk rá a következő iterációs lépésre.
\end{enumerate}

Az eljárás nem csak minimum-, hanem maximumfeladatokra is használható kis
változtatással. Tehát amikor B\&B eljárással oldunk meg egy feladatot, akkor
meg kell adni
\begin{itemize}[nosep]
  \item az $\Omega$ halmazt;
  \item a $\phi$ szétválasztó függvényt;
  \item a $g$ korlátozó függvényt; és
  \item a levélválasztási stratégiát.
\end{itemize}
%------------------------------------------------------------------------------
\minisec{A hátizsák feladat}
A hátizsák feladatnak több formája van. Mi most a bináris hátizsák feladattal
fogunk foglalkozni, amelyről bizonyították, hogy NP-nehéz, tehát nem várható
hatékony eljárás a megoldására.

Adott egy hátizsák és különböző tárgyak. Legyen
\begin{itemize}[nosep]
  \item $m$ a tárgyak száma;
  \item $w_{j}$ a $j$-edik tárgy súlya $\left(j=1, 2, \ldots, <m\right)$;
  \item $c_{j}$ a $j$-edik tárgy értéke $\left(j=1, 2, \ldots, <m\right)$;
  \item $b$ a hátizsák súlykorlátja;
  \item $x_{j}=1$, ha a $j$-edik tárgy bekerül a zsákba és $x_{j}=0$ ha nem.
\end{itemize}

Feltételezzük, hogy minden tárgyból csak egy van. Ha nem, akkor a példányokat
különböző tárgyaknak tekintjük. A cél az, hogy a tárgyakból olyan rakományt
állítsunk össze, melyet a hátizsák még elbír és összértéke maximális.
%------------------------------------------------------------------------------
\minisec{Információkigyűjtés}
A következő példák a halmazlefedési feladat tipikus alkalmazásai.

Adott $m$ fájl különböző információtartalmakkal. Fájlok összemásolásával egy
minél rövidebb olyan új fájlt akarunk képezni, amely $n$ féle, az eredeti
fájlokban megtalálható információt tartalmaz.
%------------------------------------------------------------------------------
\minisec{Termelésszervezés}
Adott $n$ elvégzendő feladat és $m$ munkás, melyek mindegyike a feladatok közül
bizonyosokat el tud végezni. Válasszunk ki minél kevesebb (összköltségű)
munkást, akik együtt az $n$ munkát elvégzik.
