Algoritmusok és adatszerkezetek II. ‒ Kidolgozott tételsor
==========================================================

Nyilatkozat
-----------

Ez egy **nem hivatalos** segédanyag, kizárólag saját felelősségre
használható.

A segédanyagot még 2015-ben írtam a kurzus hallgatójaként, azóta tartalmilag
nem változtattam rajta. Erősen ajánlott alaposan átnézni és frissíteni.

Licenc
------

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img
alt="Creative Commons Licenc" style="border-width:0"
src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br /><span
xmlns:cc="http://creativecommons.org/ns#" property="cc:attributionName">Mezőfi
Dávid</span> <span xmlns:dct="http://purl.org/dc/terms/"
href="http://purl.org/dc/dcmitype/Text" property="dct:title"
rel="dct:type">Algoritmusok és adatszerkzetek II. ‒ Kidolgozott
tételsor</span> című műve <a rel="license"
href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Nevezd
meg! - Így add tovább! 4.0 Nemzetközi Licenc</a> alatt van.

Fordítás
--------

A fordításhoz szükséged lesz a `texlive` LaTeX-disztribúcióra.

```{.bash}
git clone https://gitlab.com/davidmezofi/alga-2.git
cd alga-2 && make
```

A `make release` (vagy csak simán `make`) paranccsal az `alga-2.tex` fájl
fordítása utána csak az `alga-2.pdf` marad meg, míg a `make all` parancs
meghagyja a fordítás során keletkező egyéb fájlokat is (`.aux`, `.toc`, stb.).
Ezek a fájlok gyorsítják a későbbi fordítások futási idejét. A `make clean`
minden a fordítás során keletkező fájlt eltávolít.
