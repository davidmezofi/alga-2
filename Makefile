chapters = $(wildcard chap*.tex)
extra = aux loa log out toc

release: all
	rm -f $(addprefix alga-2.,$(extra))

all: alga-2.pdf $(chapters)

alga-2.pdf: alga-2.tex $(chapters)
	pdflatex -jobname=$(basename $@) $<
	pdflatex -jobname=$(basename $@) $<
	pdflatex -jobname=$(basename $@) $<

clean:
	rm -f alga-2.pdf $(addprefix alga-2.,$(extra))

.PHONY: release all
