\chapter{Mintaillesztés}
%------------------------------------------------------------------------------
\minisec{Mintaillesztés automatával}
\begin{mydefin}[prefix]
  Az $U$ szó kezdőszelete vagy \emph{prefixe} $V$-nek $\left(U\sqsubset
  V\right)$, ha létezik $W\in\Sigma^{*}$ úgy, hogy $UW=V$.
\end{mydefin}
\begin{mydefin}[szuffix]
  Az $U$ szó végződés vagy \emph{szuffixe} $V$-nek $\left(U\sqsupset V\right)$,
  ha létezik $W\in\Sigma^{*}$ úgy, hogy $WU=V$.
\end{mydefin}

Az $X\in\Sigma^{n}$ szó első $i$ betűjéből álló prefixére az
$X_{i}=X{\left[1\ldots i\right]}$ jelölést használjuk: $X_{i}=x_{1}x_{2}\ldots
x_{n}$.

$T, P\in\Sigma^{*}$, $T$ a szöveg, $P$ a minta. Mi a legkisebb (vagy másik
problémában az összes) olyan $i$, amelyre $T{\left[i+1\ldots i+m\right]}=P$,
ahol $m=\left|P\right|$ a minta hossza? Általában
$m=\left|P\right|\ll\left|T\right|=n$. Ekkor azt mondjuk, hogy a $P$ minta $i$
eltolással illeszkedik a $T$-re. A naiv megoldás $\mathcal{O}{\left(m\cdot
n\right)}$-es.

Legyen
\begin{equation*}
  M=\left(Q, q_{0}, F, P, \delta\right)
\end{equation*}
olyan véges determinisztikus automata, amely a $\Sigma^{*}P$ nyelvet ismeri
fel! Mivel $\Sigma^{*}P$ az összes olyan szavak halmaza, amelyek az $P$ mintára
végződnek, ezért $T{\left[1\ldots i\right]}$-t pontosan akkor ismeri fel az
automata, ha a $P$ minta $i-m$ eltolással illeszkedik $T$-re.
\begin{equation}
  \delta{\left(q, x\right)}=\begin{cases}
    q+1,&\text{ha }x=P{\left[q+1\right]};\\
  \max\left\{k\colon P_{k}\sqsupset P_{q}x\right\},&\text{ha }x\neq
    P{\left[q+1\right]}.
  \end{cases}
\end{equation}

Mottó: a minta mennyire szuffixe annak, amit látunk?
\begin{algorithm}[H]
  \caption{Mintaillesztés automatával}
  \begin{algorithmic}[1]
    \Procedure{Mintaillesztés}{$T, P$}
      \State$q\gets 0$
      \State$n\gets\left|T\right|$
      \State$m\gets\left|P\right|$
      \For{$i\gets 1, i\le n, i\gets i+1$}
        \State$q\gets\delta{\left(q, T{\left[i\right]}\right)}$
      \EndFor
    \EndProcedure
  \end{algorithmic}
\end{algorithm}
Az algoritmus futási ideje adott automata mellett
$\mathcal{O}{\left(n\right)}$.
%------------------------------------------------------------------------------
\minisec{Knuth--Morris--Pratt-algoritmus}
Ha egy helyen a minta egy kezdőszelete illeszkedik, akkor a következő néhány
elemnél nem feltétlen kell ellenőrizni az illeszkedést, hanem a mintától és az
aktuális illeszkedéstől függően lehet előre léptetni az összehasonlító
algoritmust. A léptetés mértéke előre kiszámítható a mintára.

A minta \emph{prefix függvénye}, $\textsc{Pre}\colon\left\{1, 2, \ldots,
m\right\}\rightarrow\left\{0, 1, \ldots, m-1\right\}$ a következőképpen adható
meg:
\begin{equation}
  \textsc{Pre}{\left(q\right)}=\max\left\{k\colon k<q\text{ és }S_{k}\sqsupset
  S_{q}\right\}\quad\left(q=1, 2, \ldots, m\right).
\end{equation}
\begin{algorithm}[H]
  \caption{Knuth--Morris--Pratt-algoritmus}
  \begin{algorithmic}[1]
    \Procedure{KMP}{$T, P$}
      \State$n\gets\left|T\right|$
      \State$m\gets\left|P\right|$
      \State$\textsc{pre}\gets\Call{Prefix}{P}$\Comment{Legrosszabb esetben
        $\mathcal{O}{\left(m\right)}$ idő alatt fut le.}
      \State$q\gets 0$
      \For{$i\gets 1, i\le n, i\gets i+1$}
        \While{$q>0$ és $P{\left[q+1\right]}\neq T{\left[i\right]}$}
          \State$q\gets\textsc{pre}{\left[q\right]}$
        \EndWhile
        \If{$P{\left[q+1\right]}=T{\left[i\right]}$}
          \State$q\gets q+1$
        \EndIf
        \If{$q=m$}
          \State\Call{Illeszkedik}{$i-m+1$}
          \State$q\gets\textsc{pre}{\left[q\right]}$\Comment{Következő illeszkedés keresése.}
        \EndIf
      \EndFor
    \EndProcedure
    \end{algorithmic}
\end{algorithm}
%------------------------------------------------------------------------------
\minisec{Rabin--Karp-algoritmus}
Az algoritmus a $\Sigma$ feletti szavakat, egy $\left|\Sigma\right|=d$ alapú
számrendszerben felírt számként tekinti. Ekkor a $P$ mintát egyértelműen
megadja a hozzárendelt szám értéke, amit jelöljön $p$. Ez a $p$ érték
kiszámítható $\mathcal{O}{\left(m\right)}$ időben. Az algoritmus alapötlete,
hogy a $T$ szövegre, minden $i$-re kiszámolva a $T{\left[i+1\ldots i+m\right]}$
számértéket azon esetekben, ahol ez a szám $p$, illeszkedik a minta. Ezt
hatékonyán számíthatjuk, mivel $T{\left[i+2\ldots
i+m+1\right]}=\left(T{\left[i+1\ldots
i+m\right]}-d^{m}T{\left[i+1\right]}\right)d+T{\left[i+m+1\right]}$, így az
első érték után a többiek mindegyike konstans időben kiszámítható.

A módszerrel az a probléma, hogy a létrejövő számok túl nagyok, így nem
ábrázolhatóak a számítógépen a szokásos számtípusokkal. A megoldás az, hogy a
számok helyett csak egy megfelelően nagy, de még kezelhető $q$ érték szerinti
osztásmaradékukat használjuk.

Természetesen így egy olyan algoritmust kapunk, amely esetén a minta
illeszkedik válasz hamis lehet, ha a két szám maradéka megegyezik. Ezért a
maradékosztályok megegyezése esetén, a helyességet ellenőrizni kell.

Viszont az algoritmus gyors elutasítási heurisztikaként jól használható, mivel
ha azt kapjuk, hogy a minta nem illeszkedik, akkor valóban nincs illeszkedés.
